export interface QR {
    $key?:string;
    NombreUsuario:string;
    Serie?:number;
    Foto?:string;
    Video?:string;
    Archivo?:string;
    Texto?:string;
}
