import { Component, OnInit } from '@angular/core';
import { Cliente } from 'src/app/interfaces/usuario.model';
import { AuthService } from "../../../services/auth.service"

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

  constructor(public authService: AuthService) { }

  ngOnInit() {
  }

}
