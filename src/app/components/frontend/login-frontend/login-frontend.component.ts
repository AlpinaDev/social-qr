import { Component, OnInit } from '@angular/core';
import { AuthService } from "../../../services/auth.service"
import { ClienteService } from "../../../services/cliente.service"
//versionImportante  Agrgamos el crudservice
import { CrudService } from '../../../services/crud.service';  // CRUD services API
import { User } from '../../../services/user.model';
import { NgForm } from '@angular/forms';
import { AngularFirestore } from '@angular/fire/firestore';
@Component({
  selector: 'app-login-frontend',
  templateUrl: './login-frontend.component.html',
  styleUrls: ['./login-frontend.component.css']
})
export class LoginFrontendComponent implements OnInit {

list: User[];
  isRegistro: boolean;

//versionImportante
//agregamos al constructor  private service: CrudService,private clienteService:ClienteService, private firestore: AngularFirestore para lograr impactar en cloud firestore
  constructor(public authService: AuthService, private service: CrudService,private clienteService:ClienteService, private firestore: AngularFirestore) {
    if(window.location.href.indexOf("/registro") !== -1){
      this.isRegistro = true;
    }else{
      this.isRegistro = false;
    }
   
  }

  ngOnInit() {
    this.resetForm();
  }
//versionImportante
//this.service.formData hace referencia al servicio que se encuentra en src\app\services\crud.service.ts. No se si este codigo y el onsubmit deberian estar en el servicio o parte de ellos
  resetForm(form?: NgForm) {
    if (form != null)
      form.resetForm();
    this.service.formData = {
      uid: null,
      email: '',
      displayName: '',
      photoURL: '',
      emailVerified: false,
      password:''
    }
  }
  
  //versionImportante *************
//Manejo de LOCALSTORAGE y async para evaluar el "resultado" del metodo this.authService.SignUp(form) del archivo ...\src\app\services\auth.service.ts
  // EJEMPLO BASICO de async
  //asyncExample().then(users => {
  //   console.log(users)
  // })
  // .catch(err => console.error(err))

 registrarUsuario(form){
 //versionImportante *************
//aqui se le pasa la variable form que contiene todos los campos del formulario en form.value. Ejemplo form.value.email
  this.authService.SignUp(form).then(result => {
   if(result){
	   //versionImportante
		  //obtengo los datos de la sesion de local storage guardada en  ...\src\app\services\auth.service.ts
        let cliente= JSON.parse(localStorage.getItem('registroCliente'));
        this.clienteService.addClientes(cliente.displayName, cliente.email);
		//versionImportante
		  //remuevo el item de sesion storage
        localStorage.removeItem('registroCliente');}
})
.catch(err => alert("error al registrarse >"+err));
//versionImportante *************
//asi podriamos utilizar los campos del formulario
    // this.onSubmit(form);
    // this.clienteService.addClientes(form.value.displayName,form.value.email);


//versionImportante
//es para traer datos desde firestore
  // var ver =  this.service.getEmployees();
  // this.service.getEmployees().subscribe(actionArray => {
  //       this.list = actionArray.map(item => {
  //         return {
  //           uid: item.payload.doc.id,
  //           ...item.payload.doc.data()
  //         } as User;
  //       })
  //     });
}
//versionImportante
//aqui subimos datos a firestore, al documento o Clientes
  onSubmit(form: NgForm) {
    let data = Object.assign({}, form.value);
    delete data.id;
    if (form.value.id == null)
      this.firestore.collection('clientes').add(data);
    else
      this.firestore.doc('clientes/' + form.value.id).update(data);
    this.resetForm(form);
  }

}
