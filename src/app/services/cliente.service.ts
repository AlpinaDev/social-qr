import { Injectable } from '@angular/core';
import { AngularFireDatabase, AngularFireList } from 'angularfire2/database';
import * as firebase from 'firebase/app';
import { Router } from '@angular/router';
//versionImportante
//Se hace consulta y agrega registro a la coleccion llamada clientes en la base de datos real time de firebase
//esto se usa en el archivo src\app\components\frontend\login-frontend\login-frontend.component.ts
@Injectable({
  providedIn: 'root'
})
export class ClienteService {

  listadoClientes: AngularFireList<any>;

  constructor(
    public firebase:AngularFireDatabase,
    private location: Router
  ){}

  ngOnInit() {

  }
  /**
   * Devuelve el listado de los clientes.
   */
  getClientes(){
    return this.listadoClientes = this.firebase.list("/Clientes");
  }
/**
   * agrega al listado de los clientes. Solo nombre y email. pero podria ser mas completo con mas datos. Se pasa un json
   */
  addClientes(nombre: string, email: string): void {
    this.listadoClientes = this.firebase.list("/Clientes");
    this.listadoClientes.push({ nombre: nombre, email: email });
  }



}
