import { AngularFirestore } from '@angular/fire/firestore';
import { Injectable } from '@angular/core';
import { User } from './user.model';

@Injectable({
  providedIn: 'root'
})
export class CrudService {
	//versionImportante
//definimos la variable formData que utilizaremos en el Frontend ..\src\app\components\frontend\login-frontend\login-frontend.component.html para bindear el formulario y enviar datos a cloud firestore por medio del metodo onSubmit en ..\src\app\components\frontend\login-frontend\login-frontend.component.ts
  formData: User;

  constructor(private firestore: AngularFirestore) { }
//versionImportante
//esto se consume en el ejemplo comentado de ..\src\app\components\frontend\login-frontend\login-frontend.component.ts
  getEmployees() {
    return this.firestore.collection('clientes').snapshotChanges();
  }
}
